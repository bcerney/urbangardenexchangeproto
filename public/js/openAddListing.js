$(document).ready(function() {
	
	// console.log(firebase);
	//   var listingRef = firebase.database().ref('listing');
	//   console.log('listing', listingRef);
	//   console.log(firebase);

	//var listingRef = firebase.database().ref();
	
	var query = firebase.database().ref("listing").orderByChild("datetime");
	//console.log(query);
	query.once("value")
	  .then(function(snapshot) {
	    snapshot.forEach(function(childSnapshot) {
	      // key will be "ada" the first time and "alan" the second time
			//console.log(childSnapshot);
	      //var key = childSnapshot.key;
		  //console.log(key);
	      // childData will be the actual contents of the child
		  var childData = childSnapshot.val();
		  // console.log(childData);
		  // console.log(childData.produceItem);
		  displayListing(childData.datetime, childData.username, childData.email, childData.transaction, childData.produceItem, childData.quantity, childData.unit, childData.message);
	  });
	});
	
  
	$('#submitListing').on("click", function(event) {
		var listingNumber = Math.round((Math.random()*1000) + 1);
		Math.floor(Math.random() * 6) + 1  
	  
		var currentdate = new Date(); 
		    var datetime = "Posted: " + currentdate.getDate() + "/"
		                + (currentdate.getMonth()+1)  + "/" 
		                + currentdate.getFullYear() + " @ "  
		                + currentdate.getHours() + ":"  
		                + currentdate.getMinutes() + ":" 
		                + currentdate.getSeconds();
						
		var username = $('#username').val();
		var email = $('#email').val();
		var transaction = $('#transaction option:selected').val();
		var produceItem = $('#produceItem').val();
		var quantity = $('#quantity').val();
		var unit = $('#unit').val();
		var message = $('#message').val();
	  
		addListing(listingNumber, datetime, username, email, transaction, produceItem, quantity, unit, message);
	  	location.reload();
	});
  
	function addListing(listingNumber, datetime, username, email, transaction, produceItem, quantity, unit, message) {
		firebase.database().ref('listing/' + listingNumber).set({
			datetime: datetime,
			username: username,
			email: email,
			transaction: transaction,
			produceItem: produceItem,
			quantity: quantity,
			unit: unit,
			message: message
		})
		// .then(function() {
		// 	return listingRef.once("value");
		// })
		// .then(function(snapshot) {
		// 	var data = snapshot.val();
		// 	console.log(data);
			// console.log(data.listingNumber);
			// console.log(data.username);
			// console.log(data.produceItem);
			

	};
	
	function displayListing(datetime, username, email, transaction, produceItem, quantity, unit, message) {
		var html = '<div class="" ' +
					'style="border-radius: 5px;background-color:#f1f2f2;margin: 0px 5px 0px 5px;max-width: 70%; margin: 0 auto;">' +
					'<p>' + datetime + '</p>' +
					'<p><strong>' + username + '</strong>' + ' (' + email +')' + '</p>' +
					'<p><strong>' + transaction + ': </strong>' + produceItem +' (' + quantity + ' ' + unit + ')'+ '</p>' +
					'<p>' + message + '</p>' +
				'</div>';
		console.log(html);
		$('#listings').prepend(html);
	}
	
	
  
});

	// var rootRef = firebase.database().ref();
// 	rootRef.once("value")
// 	  .then(function(snapshot) {
// 		  console.log(snapshot);
// 	    var key = snapshot.key; // null
// 	    var childKey = snapshot.child("users/ada").key; // "ada"
// 	  });
	  
	// Write and then read back a JavaScript object from the Database.
	// listingRef.set({ name: "Ada", age: 36 })
	//   .then(function() {
	//    return listingRef.once("value");
	//   })
	//   .then(function(snapshot) {
	//     var data = snapshot.val();
	// 	console.log(data);
	//     data is { "name": "Ada", "age": 36 }
	//     data.name === "Ada"
	//     data.age === 36
	//   });

// firebase.database().ref('/tasks/').orderByChild('uid').equalTo(userUID)
//
// return firebase.database().ref('/tasks/').orderByChild('uid').equalTo(userUID).once('value').then(function(snapshot) {
//   var username = snapshot.val().username;
//   // ...
// });
//
// contactsRef.on("child_added", function(snap) {
// 	console.log("added", snap.key(), snap.val());
// 	$('#contacts').append(contactHtmlFromObject(snap.val()));
// });


// function writeUserData(userId, name, email, imageUrl) {
	//   firebase.database().ref('users/' + userId).set({
		//     username: name,
		//     email: email,
		//     profile_picture : imageUrl
		//   });
		// }